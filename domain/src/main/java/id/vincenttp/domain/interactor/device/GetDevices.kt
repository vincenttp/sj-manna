package id.vincenttp.domain.interactor.device

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository

/**
 * Created by vincenttp on 2019-09-02.
 */
class GetDevices(private val deviceRepository: DeviceRepository) : BaseUseCase<List<DeviceModel>, Void?>() {
    override suspend fun build(params: Void?) = deviceRepository.getDevices()
}