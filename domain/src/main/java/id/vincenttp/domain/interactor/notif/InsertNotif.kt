package id.vincenttp.domain.interactor.notif

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.domain.repository.NotifRepository

/**
 * Created by vincenttp on 2019-09-28.
 */
class InsertNotif(private val notifRepository: NotifRepository) : BaseUseCase<Unit, InsertNotif.Params>() {
    override suspend fun build(params: Params?) = notifRepository.insert(params!!.notifModel)

    data class Params(var notifModel: NotifModel)
}