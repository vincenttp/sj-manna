package id.vincenttp.domain.interactor.device

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository
import kotlinx.coroutines.Job

/**
 * Created by vincenttp on 2019-09-27.
 */
class UpdateDevice(private val deviceRepository: DeviceRepository) : BaseUseCase<Job, UpdateDevice.Params>() {
    override suspend fun build(params: Params?) = deviceRepository.updateDevice(params!!.deviceModel)

    data class Params(var deviceModel: DeviceModel)
}