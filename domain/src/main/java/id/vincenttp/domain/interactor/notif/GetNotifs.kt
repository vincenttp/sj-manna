package id.vincenttp.domain.interactor.notif

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.domain.repository.NotifRepository

/**
 * Created by vincenttp on 2019-09-28.
 */
class GetNotifs(private val notifRepository: NotifRepository) : BaseUseCase<List<NotifModel>, Void?>() {
    override suspend fun build(params: Void?) = notifRepository.gets()
}