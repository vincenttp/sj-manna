package id.vincenttp.domain.interactor.device

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository

/**
 * Created by vincenttp on 2019-09-25.
 */
class GetDevice(private val deviceRepository: DeviceRepository) : BaseUseCase<DeviceModel, GetDevice.Params>() {
    override suspend fun build(params: Params?): DeviceModel = deviceRepository.getDevice(params!!.address)

    data class Params(val address: String)
}