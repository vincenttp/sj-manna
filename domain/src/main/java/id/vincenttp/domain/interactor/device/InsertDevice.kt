package id.vincenttp.domain.interactor.device

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository

/**
 * Created by vincenttp on 2019-09-02.
 */
class InsertDevice(private val deviceRepository: DeviceRepository) : BaseUseCase<Unit, InsertDevice.Params>() {
    override suspend fun build(params: Params?) = deviceRepository.insertDevice(params!!.deviceModel)

    data class Params(var deviceModel: DeviceModel)
}