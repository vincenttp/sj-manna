package id.vincenttp.domain.interactor.device

import id.vincenttp.domain.BaseUseCase
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository

/**
 * Created by vincenttp on 2019-09-27.
 */
class DeleteDevice(private val deviceRepository: DeviceRepository) : BaseUseCase<Int, DeleteDevice.Params>() {
    override suspend fun build(params: Params?) = deviceRepository.deleteDevice(params!!.deviceModel)

    data class Params(var deviceModel: DeviceModel)
}