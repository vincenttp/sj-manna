package id.vincenttp.domain.repository

import id.vincenttp.domain.entities.DeviceModel
import kotlinx.coroutines.Job

/**
 * Created by vincenttp on 2019-09-02.
 */
interface DeviceRepository {
    suspend fun insertDevice(deviceModel: DeviceModel)
    suspend fun getDevices(): List<DeviceModel>
    suspend fun getDevice(address: String): DeviceModel
    suspend fun updateDevice(deviceModel: DeviceModel): Job
    suspend fun deleteDevice(deviceModel: DeviceModel): Int
    fun changeDevice(deviceModel: DeviceModel)
}