package id.vincenttp.domain.repository

import id.vincenttp.domain.entities.NotifModel

/**
 * Created by vincenttp on 2019-09-28.
 */
interface NotifRepository {
    fun insert(notifModel: NotifModel)
    suspend fun gets(): List<NotifModel>
}