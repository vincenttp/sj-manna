package id.vincenttp.domain.entities

import java.io.File
import java.io.Serializable

/**
 * Created by vincenttp on 2019-08-29.
 */
data class DeviceModel(
        var name: String,
        var address: String,
        var state: Boolean,
        var isRinging: Boolean,
        var image: String
) : Serializable