package id.vincenttp.domain.entities

import java.io.Serializable

/**
 * Created by vincenttp on 2019-08-29.
 */
data class NotifModel(
        var id: Int,
        var address: String,
        var message: String,
        var image: String,
        var lat: Double,
        var lng: Double,
        var date: Long
) : Serializable