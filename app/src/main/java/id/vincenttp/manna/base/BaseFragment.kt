package id.vincenttp.manna.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import id.vincenttp.manna.common.DialogLoading
import org.koin.android.ext.android.inject

abstract class BaseFragment<VM : BaseViewModel> : Fragment() {
    abstract val viewModel: VM
    val dialogLoading: DialogLoading by lazy {
        DialogLoading(context!!)
    }
    val firebaseAuth: FirebaseAuth by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dialogLoading = dialogLoading
        onInitViews()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onInitObservers()
    }

    protected open fun onInitViews() = Unit

    protected open fun onInitObservers() = Unit
}
