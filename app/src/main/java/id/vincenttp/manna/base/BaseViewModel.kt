package id.vincenttp.manna.base

import androidx.lifecycle.ViewModel
import id.vincenttp.manna.common.DialogLoading

abstract class BaseViewModel : ViewModel() {
    lateinit var dialogLoading: DialogLoading
}
