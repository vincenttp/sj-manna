package id.vincenttp.manna

import android.app.Application
import id.vincenttp.manna.module.apiModule
import id.vincenttp.manna.module.dataModule
import id.vincenttp.manna.module.roomModule
import id.vincenttp.manna.module.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AndroidApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@AndroidApp)
            modules(
                    listOf(
                            dataModule,
                            apiModule,
                            useCaseModule,
                            viewModelModule,
                            roomModule,
                            thirdParty
                    )
            )
        }
    }
}