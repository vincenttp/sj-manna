package id.vincenttp.manna.module

import com.google.firebase.auth.FirebaseAuth
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Created by vincenttp on 2019-08-26.
 */

val thirdParty: Module = module {
    single { FirebaseAuth.getInstance() }
}