package id.vincenttp.manna.module

import androidx.room.Room
import id.vincenttp.data.DatabaseApp
import id.vincenttp.manna.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Created by vincenttp on 2019-09-02.
 */

val roomModule = module {
    single {
        Room.databaseBuilder(androidContext(), DatabaseApp::class.java, BuildConfig.APPLICATION_ID)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }

    single { get<DatabaseApp>().deviceDao() }
    single { get<DatabaseApp>().notifDao() }
}