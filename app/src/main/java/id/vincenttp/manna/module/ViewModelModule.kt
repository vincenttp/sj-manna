package id.vincenttp.manna.module

import id.vincenttp.manna.feature.add.pairing.PairingViewModel
import id.vincenttp.manna.feature.add.success.PairingSuccessViewModel
import id.vincenttp.manna.feature.auth.login.LoginViewModel
import id.vincenttp.manna.feature.auth.register.RegisterViewModel
import id.vincenttp.manna.feature.device.DeviceViewModel
import id.vincenttp.manna.feature.device.edit.EditDeviceViewModel
import id.vincenttp.manna.feature.home.HomeViewModel
import id.vincenttp.manna.feature.notif.NotifViewModel
import id.vincenttp.manna.feature.profile.ProfileViewModel
import id.vincenttp.manna.feature.profile.edit.EditViewModel
import id.vincenttp.manna.feature.profile.password.PasswordViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by vincenttp on 2019-08-04.
 */

val viewModelModule = module {
    viewModel<RegisterViewModel>()
    viewModel<LoginViewModel>()
    viewModel<HomeViewModel>()
    viewModel<ProfileViewModel>()
    viewModel<NotifViewModel>()
    viewModel<PairingViewModel>()
    viewModel<PairingSuccessViewModel>()
    viewModel<EditDeviceViewModel>()
    viewModel<DeviceViewModel>()
    viewModel<EditViewModel>()
    viewModel<PasswordViewModel>()
}