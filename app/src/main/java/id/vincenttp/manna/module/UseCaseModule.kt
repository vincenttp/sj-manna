package id.vincenttp.manna.module

import id.vincenttp.domain.interactor.device.*
import id.vincenttp.domain.interactor.notif.GetNotifs
import id.vincenttp.domain.interactor.notif.InsertNotif
import org.koin.dsl.module
import org.koin.experimental.builder.factory

/**
 * Created by vincenttp on 2019-08-03.
 */

val useCaseModule = module {
    factory<GetDetail>()
    factory<GetRepo>()
    factory<InsertDevice>()
    factory<GetDevices>()
    factory<GetDevice>()
    factory<UpdateDevice>()
    factory<DeleteDevice>()
    factory<InsertNotif>()
    factory<GetNotifs>()
}