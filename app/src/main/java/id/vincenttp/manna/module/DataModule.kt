package id.vincenttp.manna.module

import id.vincenttp.data.repository.DeviceRepositoryImpl
import id.vincenttp.data.repository.NotifRepositoryImpl
import id.vincenttp.data.repository.UserRepositoryImpl
import id.vincenttp.domain.repository.DeviceRepository
import id.vincenttp.domain.repository.NotifRepository
import id.vincenttp.domain.repository.UserRepository
import org.koin.dsl.module

/**
 * Created by vincenttp on 2019-08-03.
 */

val dataModule = module {
    single<UserRepository> { UserRepositoryImpl(get()) }
    single<DeviceRepository> { DeviceRepositoryImpl(get()) }
    single<NotifRepository> { NotifRepositoryImpl(get()) }
}