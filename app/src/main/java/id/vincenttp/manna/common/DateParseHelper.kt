package id.vincenttp.manna.common

import android.annotation.SuppressLint
import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by vincenttp on 31/01/19.
 */
class DateParseHelper {
    companion object {
        const val BASE_FORMAT = "yyyy-MM-dd"
        const val BASE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss"
        const val FORMAT_DATE = "dd"
        const val FORMAT_MONTH = "MMM"
        const val FORMAT_FULL_MONTH = "MMMM"
        const val FORMAT_TIME_12 = "hh:mm aaa"
        const val FORMAT_TIME_24 = "HH:mm"
        const val FORMAT_DAY_DATE = "EEEE, dd MMMM yyyy"
        const val FORMAT_DAY_DATE_TIME = "EEEE, dd MMMM yyyy HH:mm"
        const val FORMAT_DATE_MONTH_YEAR = "dd MMM yyyy"
        const val FORMAT_DATE_TIME = "dd/MM/yyyy HH:mm"
        const val FORMAT_DATE_SHORT = "dd MMM"
        const val FORMAT_TIME_HOUR_MINUTE = "HH:mm"
        const val FORMAT_MONTH_YEAR = "dd/MM"

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun parse(dateString: String, format: String): Date {
            val simpleDateFormat = SimpleDateFormat(format)
            return simpleDateFormat.parse(dateString)
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getString(dateString: String, outputFormat: String): String {
            val format = SimpleDateFormat(outputFormat)
            return format.format(parse(dateString, BASE_FORMAT))
        }

        @JvmStatic
        fun getLong(dateString: String, inputFormat: String): Long =
                parse(dateString, inputFormat).time

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getCurrentDate(): String {
            val format = SimpleDateFormat(BASE_FORMAT)
            val date = Date()
            return format.format(date)
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getCurrentDateTime(): String {
            val format = SimpleDateFormat(BASE_FORMAT_FULL)
            val date = Date()
            return format.format(date)
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getDate(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_DATE)
            return format.format(parse(dateString, BASE_FORMAT))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getMonth(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_MONTH, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getFullMonth(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_FULL_MONTH, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getFullDate(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_DAY_DATE, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getFullDate(dateString: String, inputFormat: String): String {
            val format = SimpleDateFormat(FORMAT_DAY_DATE, Locale("in", "ID"))
            return format.format(parse(dateString, inputFormat))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getFullDateTime(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_DAY_DATE_TIME, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT_FULL))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getShortDate(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_DATE_SHORT, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT_FULL))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun getTime(dateString: String): String {
            val format = SimpleDateFormat(FORMAT_TIME_HOUR_MINUTE, Locale("in", "ID"))
            return format.format(parse(dateString, BASE_FORMAT_FULL))
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun isToday(dateString: String): Boolean = DateUtils.isToday(parse(dateString, BASE_FORMAT).time)

        @JvmStatic
        fun isSameDay(startDate: String, endDate: String): Boolean {
            val cal1 = Calendar.getInstance()
            val cal2 = Calendar.getInstance()
            cal1.time = parse(startDate, BASE_FORMAT)
            cal2.time = parse(endDate, BASE_FORMAT)
            return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
        }

        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun range(start: String, end: String): Int {
            val format = SimpleDateFormat(BASE_FORMAT)
            val dateStart = format.parse(start)
            val dateEnd = format.parse(end)
            val diff = dateEnd.time - dateStart.time
            println("rangerange $diff")
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
        }

        @JvmStatic
        fun getToday(): String {
            val today = Date()
            val format = SimpleDateFormat(FORMAT_MONTH_YEAR, Locale("in", "ID"))
            return format.format(today)
        }

        @JvmStatic
        fun getDateTime(date: String): String {
            val format = SimpleDateFormat(FORMAT_DATE_TIME, Locale("in", "ID"))
            return format.format(parse(date, BASE_FORMAT_FULL))
        }

        @JvmStatic
        fun getDateMonthYear(date: String): String {
            val format = SimpleDateFormat(FORMAT_DATE_MONTH_YEAR, Locale("in", "ID"))
            return format.format(parse(date, BASE_FORMAT_FULL))
        }

        @JvmStatic
        fun getMonthYear(date: Long): String {
            val format = SimpleDateFormat(FORMAT_MONTH_YEAR, Locale("in", "ID"))
            return format.format(Date(date))
        }

        @JvmStatic
        fun getHourMinute24(date: Long): String {
            val format = SimpleDateFormat(FORMAT_TIME_24, Locale("in", "ID"))
            return format.format(Date(date))
        }
    }
}