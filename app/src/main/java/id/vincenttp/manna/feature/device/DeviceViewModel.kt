package id.vincenttp.manna.feature.device

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.interactor.device.GetDevice
import id.vincenttp.domain.interactor.device.UpdateDevice

class DeviceViewModel(private val getDevice: GetDevice, private val updateDevice: UpdateDevice) : ViewModel() {
    val _deviceList = MutableLiveData<DeviceModel>()
    val deviceList: LiveData<DeviceModel>
        get() = _deviceList

    fun getDevice(address: String) {
        getDevice.addParams(GetDevice.Params(address))
                .onSuccess { _deviceList.postValue(it) }
                .execute(viewModelScope)
    }

    fun updateDevice(deviceModel: DeviceModel){
        updateDevice.addParams(UpdateDevice.Params(deviceModel))
                .onSuccess {
                    getDevice(deviceModel.address)
                }
                .execute(viewModelScope)
    }
}
