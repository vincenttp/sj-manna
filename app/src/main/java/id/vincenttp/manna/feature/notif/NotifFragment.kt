package id.vincenttp.manna.feature.notif

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.databinding.FragmentNotifBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotifFragment : BaseFragment<NotifViewModel>() {

    companion object {
        fun newInstance() = NotifFragment()
    }

    override val viewModel by viewModel<NotifViewModel>()

    private val adapter = NotifAdapter {
        val gmmIntentUri: Uri = Uri.parse("geo:0,0?q=${it.lat},${it.lng}()")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        if (mapIntent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(mapIntent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentNotifBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.rvNotif.adapter = adapter
        return binding.root
    }

    override fun onInitObservers() {
        super.onInitObservers()
        viewModel.notifList.observe(this, Observer {
            adapter.submitList(it)
        })
    }
}
