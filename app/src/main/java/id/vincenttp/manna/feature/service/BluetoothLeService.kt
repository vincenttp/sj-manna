package id.vincenttp.manna.feature.service

import android.app.*
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.media.RingtoneManager.*
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MIN
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.LocationServices
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.domain.repository.DeviceRepository
import id.vincenttp.domain.repository.NotifRepository
import id.vincenttp.manna.R
import id.vincenttp.manna.feature.MainActivity
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.nio.ByteBuffer
import java.util.*

class BluetoothLeService : Service(), KoinComponent {
    companion object {
        const val EXTRA_DEVICE = "EXTRA_DEVICE"
        val SERVICE_UUID_CUSTOM: UUID = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb")
        val SERVICE_UUID_RING_PHONE: UUID = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb")
        val SERVICE_UUID_ALERT: UUID = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb")

        val CHAR_BUTTON = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb")
        val CHAR_CONNECTION = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb")
        val CHAR_RING = UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb")

        val DESC_BUTTON = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

        val ALERT_HIGH = 0x02

        val CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

        private val DEFAULT_PASSWORD = byteArrayOf(0xA1.toByte(), 0xA2.toByte(), 0xA3.toByte(), 0xA4.toByte())

        fun convertFromInteger(i: Int): UUID {
            val MSB = 0x0000000000001000L
            val LSB = -0x7fffff7fa064cb05L
            val value = (i and -0x1).toLong()
            return UUID(MSB or (value shl 32), LSB)
        }
    }

    lateinit var bluetoothGatt: BluetoothGatt
    lateinit var bluetoothDevice: BluetoothDevice
    lateinit var immediateAlertService: BluetoothGattService
    lateinit var deviceModel: DeviceModel

    private val deviceRepository by inject<DeviceRepository>()
    private val notifRepository by inject<NotifRepository>()

    private var isAlerting = false
    private var isForceDisconnect = false

    private val bluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }
    private val bluetoothLeScanner by lazy {
        bluetoothAdapter!!.bluetoothLeScanner
    }
    private val ringtone by lazy {
        getRingtone(applicationContext, getActualDefaultRingtoneUri(applicationContext, TYPE_RINGTONE))
    }
    private val fusedLocation by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(51, getNotification())
        }

        registerReceiver(receiverBluetooth, IntentFilter().apply {
            addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        })
        registerReceiver(receiverScanBluetooth, IntentFilter().apply {
            addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
            addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
            addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)
        })
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent!!.action == BluetoothStateReceiver.DEVICE_SCAN) {
            isForceDisconnect = false
            deviceModel = intent.getSerializableExtra(EXTRA_DEVICE) as DeviceModel
            connect()
        } else if (intent.action == BluetoothStateReceiver.DEVICE_RING) {
            ringDevice()
        } else if (intent.action == BluetoothStateReceiver.DEVICE_DISCONNECTED) {
            isForceDisconnect = true
            disconnect()
        } else if (intent.action == BluetoothStateReceiver.DEVICE_STOP_RINGTONE) {
            if (ringtone.isPlaying) {
                ringtone.stop()
            }
        } else if (intent.action == BluetoothStateReceiver.DEVICE_CONNECTED) {
            isForceDisconnect = false
            deviceModel = intent.getSerializableExtra(EXTRA_DEVICE) as DeviceModel
            connect()
        } else if (intent.action == BluetoothStateReceiver.DEVICE_UPDATE) {
            deviceModel = intent.getSerializableExtra(EXTRA_DEVICE) as DeviceModel
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiverBluetooth)
        unregisterReceiver(receiverScanBluetooth)
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
                gatt: BluetoothGatt,
                status: Int,
                newState: Int
        ) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    deviceModel.state = true
                    LocalBroadcastManager
                            .getInstance(applicationContext)
                            .sendBroadcast(
                                    Intent(BluetoothStateReceiver.DEVICE_CONNECTED).apply {
                                        putExtra(EXTRA_DEVICE, deviceModel)
                                    }
                            )
                    broadcastUpdate()
                    gatt.discoverServices()
                    deviceModel.state = true
                    deviceRepository.changeDevice(deviceModel)
                    bluetoothLeScanner.stopScan(leScanCallback)
                    println("gattCallback STATE_CONNECTED")
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    deviceModel.state = false
                    LocalBroadcastManager
                            .getInstance(applicationContext)
                            .sendBroadcast(
                                    Intent(BluetoothStateReceiver.DEVICE_DISCONNECTED).apply {
                                        putExtra(EXTRA_DEVICE, deviceModel)
                                    }
                            )
                    broadcastUpdate()

                    deviceModel.state = false
                    deviceRepository.changeDevice(deviceModel)
                    addToNotificationTable()

                    if (!ringtone.isPlaying && !isForceDisconnect) {
                        with(NotificationManagerCompat.from(applicationContext)) {
                            // notificationId is a unique int for each notification that you must define
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                createNotificationChannel(NotificationChannel("sini", "Sini", NotificationManager.IMPORTANCE_DEFAULT))
                            }
                            notify(2, notificationBuilder("Disconnected").build())
                        }
                        ringtone.play()
                    }

                    if (!isForceDisconnect) {
                        bluetoothLeScanner.startScan(leScanCallback)
                    }
                    println("gattCallback STATE_DISCONNECTED")
                }
            }
            println("gattCallback onConnectionStateChange $newState $status")
        }

        // New services discovered
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            for (gattService in gatt.services) {
                if (gattService.uuid == SERVICE_UUID_CUSTOM) {
                    val characteristic = getCharacteristic(gatt, SERVICE_UUID_CUSTOM, CHAR_CONNECTION)
                    setCharacteristicNotification(gatt, characteristic!!, true)
                    characteristic.value = DEFAULT_PASSWORD
                    if (gatt.writeCharacteristic(characteristic)) {
                        println("writeCharacteristic okokok")
                    }
                }

                if (gattService.uuid == SERVICE_UUID_ALERT) {
                    immediateAlertService = gattService
                }
            }
            println("gattCallback onServicesDiscovered")
        }

        // Result of a characteristic read operation
        override fun onCharacteristicRead(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
        ) {
            println("gattCallback onCharacteristicRead $status ${characteristic.uuid}")
            if (CHAR_BUTTON == characteristic.uuid) {
                val data = characteristic.value
                println("gattService ${ByteBuffer.wrap(data)}")
            }
        }

        override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
            println("gattCallback onReadRemoteRssi")
        }

        override fun onCharacteristicWrite(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?,
                status: Int
        ) {
            if (characteristic!!.service.uuid == SERVICE_UUID_CUSTOM && characteristic.uuid == CHAR_CONNECTION) {
                println("gattCallback onCharacteristicWrite keypressed")
                val characteristicButton = getCharacteristic(gatt, SERVICE_UUID_RING_PHONE, CHAR_BUTTON)
                gatt!!.setCharacteristicNotification(characteristicButton, true)
                val descriptor = characteristicButton!!.getDescriptor(DESC_BUTTON)
                descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                gatt.writeDescriptor(descriptor)
            }
            println("gattCallback onCharacteristicWrite ${characteristic.uuid}")
        }

        override fun onPhyUpdate(gatt: BluetoothGatt?, txPhy: Int, rxPhy: Int, status: Int) {
            println("gattCallback onPhyUpdate")
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
            println("gattCallback onMtuChanged")
        }

        override fun onReliableWriteCompleted(gatt: BluetoothGatt?, status: Int) {
            println("gattCallback onReliableWriteCompleted")
        }

        override fun onDescriptorWrite(
                gatt: BluetoothGatt?,
                descriptor: BluetoothGattDescriptor?,
                status: Int
        ) {
            val characteristic = gatt!!
                    .getService(SERVICE_UUID_RING_PHONE)
                    .getCharacteristic(CHAR_BUTTON)
            gatt.readCharacteristic(characteristic)
            println("gattCallback onDescriptorWrite $status ${descriptor!!.characteristic}")
        }

        override fun onCharacteristicChanged(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic?
        ) {
            if (characteristic!!.uuid == CHAR_BUTTON) {
                val keyPressValue = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)

                println("gattCallback onCharacteristicChanged keyPressValue $keyPressValue")

                if (keyPressValue == 2 || keyPressValue == 8) {
                    if (!ringtone.isPlaying) {
                        with(NotificationManagerCompat.from(applicationContext)) {
                            // notificationId is a unique int for each notification that you must define
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                createNotificationChannel(NotificationChannel("sini", "Sini", NotificationManager.IMPORTANCE_DEFAULT))
                            }
                            notify(2, notificationBuilder("Calling").build())
                        }
                        ringtone.play()
                    }
                }
            }
            println("gattCallback onCharacteristicChanged ${characteristic!!.uuid}")
        }

        override fun onDescriptorRead(
                gatt: BluetoothGatt?,
                descriptor: BluetoothGattDescriptor?,
                status: Int
        ) {
            println("gattCallback onDescriptorRead")
        }

        override fun onPhyRead(gatt: BluetoothGatt?, txPhy: Int, rxPhy: Int, status: Int) {
            println("gattCallback onPhyRead")
        }
    }

    private val leScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            println("gattCallback onScanResult ${result.device.address} ${deviceModel.address}")
            if (result.device.address == deviceModel.address) {
                connect()
            }
        }

        override fun onScanFailed(errorCode: Int) {
            println("gattCallback onScanFailed $errorCode")
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            println("gattCallback onBatchScanResults $results")
        }
    }

    private fun notificationBuilder(message: String): NotificationCompat.Builder {
        val action = PendingIntent.getActivity(
                this,
                0,
                Intent(this, MainActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    putExtra("TYPE", "STOP_RINGTONE")
                },
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        return NotificationCompat.Builder(this, "sini")
                .setSmallIcon(R.drawable.ic_stat_notifications)
                .setContentTitle("${deviceModel.name} $message")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(action)
                .setAutoCancel(true)
    }

    private fun ringDevice() {
        immediateAlert(ALERT_HIGH)
        println("gattCallback ringDevice")
    }

    private fun getCharacteristic(bluetoothgatt: BluetoothGatt?, serviceUuid: UUID, characteristicUuid: UUID): BluetoothGattCharacteristic? {
        if (bluetoothgatt != null) {
            val service = bluetoothgatt.getService(serviceUuid)
            if (service != null) {
                return service.getCharacteristic(characteristicUuid)
            }
        }
        return null
    }

    private fun setCharacteristicNotification(bluetoothgatt: BluetoothGatt, bluetoothgattcharacteristic: BluetoothGattCharacteristic, flag: Boolean) {
        bluetoothgatt.setCharacteristicNotification(bluetoothgattcharacteristic, flag)
        val descriptor = bluetoothgattcharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG)
        if (descriptor != null) {
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bluetoothgatt.writeDescriptor(descriptor)
        }
    }

    fun immediateAlert(alertType: Int) {
        val byCallData = byteArrayOf(0x2)
        if (isAlerting) {
            byCallData[0] = 0x0
        }
        if (::bluetoothGatt.isInitialized) {
            val char = getCharacteristic(bluetoothGatt, SERVICE_UUID_ALERT, CHAR_RING)
            bluetoothGatt.setCharacteristicNotification(char, true)
            char!!.value = byCallData

            if (bluetoothGatt.writeCharacteristic(char)) {
                isAlerting = !isAlerting
                deviceModel.isRinging = isAlerting
                deviceRepository.changeDevice(deviceModel)
                broadcastUpdate()
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun getNotification(): Notification {
        val channelId = "BuddyGuard"
        val notificationChannel = NotificationChannel(channelId, "tracking", NotificationManager.IMPORTANCE_NONE)
        notificationChannel.lightColor = ContextCompat.getColor(applicationContext, R.color.colorPrimary)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel)

        val builder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_notifications)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)

        return builder.build()
    }

    fun connect() {
        bluetoothDevice = bluetoothAdapter!!.getRemoteDevice(deviceModel.address)
        bluetoothGatt = bluetoothDevice.connectGatt(applicationContext, true, gattCallback)
        println("connect")
    }

    fun disconnect() {
        if (::bluetoothGatt.isInitialized) {
            bluetoothGatt.close()
            deviceModel.state = false
            deviceRepository.changeDevice(deviceModel)
            addToNotificationTable()
            if (ringtone.isPlaying) {
                ringtone.stop()
            }
            broadcastUpdate()
        }
        stopSelf()
    }

    fun broadcastUpdate() {
        LocalBroadcastManager
                .getInstance(applicationContext)
                .sendBroadcast(
                        Intent(BluetoothStateReceiver.DEVICE_UPDATE).apply {
                            putExtra(EXTRA_DEVICE, deviceModel)
                        }
                )
    }

    private val receiverBluetooth = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1!!.action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                when (p1.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    BluetoothAdapter.STATE_OFF -> {
                        println("receiverBluetooth STATE_OFF")
                    }
                    BluetoothAdapter.STATE_TURNING_OFF -> {
                        println("receiverBluetooth STATE_TURNING_OFF")

                    }
                    BluetoothAdapter.STATE_ON -> {
                        println("receiverBluetooth STATE_ON")

                    }
                    BluetoothAdapter.STATE_TURNING_ON -> {
                        connect()
                    }
                }

            }
        }
    }

    private val receiverScanBluetooth = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1!!.action == BluetoothAdapter.ACTION_SCAN_MODE_CHANGED) {
                when (p1.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR)) {
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE -> {
                        println("receiverBluetooth SCAN_MODE_CONNECTABLE_DISCOVERABLE")
                    }
                    BluetoothAdapter.STATE_TURNING_OFF -> {
                        println("receiverBluetooth STATE_TURNING_OFF")

                    }
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE -> {
                        println("receiverBluetooth SCAN_MODE_CONNECTABLE")

                    }
                    BluetoothAdapter.SCAN_MODE_NONE -> {
                        println("receiverBluetooth SCAN_MODE_NONE")

                    }
                }

            }
        }
    }

    fun addToNotificationTable(){
        fusedLocation.lastLocation.addOnSuccessListener { location : Location? ->
            notifRepository.insert(NotifModel(0, deviceModel.address, "Your ${deviceModel.name} disconnected", deviceModel.image, location!!.latitude, location.longitude,System.currentTimeMillis()))
        }

        fusedLocation.lastLocation.addOnFailureListener {
            notifRepository.insert(NotifModel(0, deviceModel.address, "Your ${deviceModel.name} disconnected", deviceModel.image, 0.0, 0.0,System.currentTimeMillis()))
        }
    }
}
