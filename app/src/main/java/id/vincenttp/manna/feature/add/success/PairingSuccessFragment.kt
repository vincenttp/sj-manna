package id.vincenttp.manna.feature.add.success

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import id.vincenttp.manna.R
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.common.ImageFilePath
import id.vincenttp.manna.databinding.FragmentPairingSuccessBinding
import id.vincenttp.manna.feature.service.BluetoothLeService
import id.vincenttp.manna.feature.service.BluetoothStateReceiver
import kotlinx.android.synthetic.main.fragment_pairing_success.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class PairingSuccessFragment : BaseFragment<PairingSuccessViewModel>(), View.OnClickListener {

    companion object {
        fun newInstance() = PairingSuccessFragment()

        const val RESULT_CODE_GALLERY = 19
    }

    override val viewModel by viewModel<PairingSuccessViewModel>()

    private val args: PairingSuccessFragmentArgs by navArgs()

    private val deviceModel by lazy {
        args.model
    }

    private var imageUri = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentPairingSuccessBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.ivPhoto.setOnClickListener(this)
        binding.onClickBack = createBackButton
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        activity?.let { LocalBroadcastManager.getInstance(it).registerReceiver(receiver, IntentFilter(BluetoothStateReceiver.DEVICE_CONNECTED)) }
        btnSave.setOnClickListener {
            deviceModel.name = etEmail.text.toString()
            deviceModel.image = imageUri
            successPairing()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        imageUri = File(ImageFilePath.getPath(context, data!!.data)).absolutePath
        ivPhoto.setImageBitmap(BitmapFactory.decodeFile(imageUri))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.let { LocalBroadcastManager.getInstance(it).unregisterReceiver(receiver) }
    }

    private val createBackButton = View.OnClickListener {
        activity!!.onBackPressed()
    }

    override fun onClick(p0: View?) {
        if (checkPermissions()) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Pilih Cover"), RESULT_CODE_GALLERY)
        }
    }

    private fun checkPermissions(): Boolean =
            if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 18)
                Toast.makeText(context, "Anda tidak memberikan akses untuk galeri.", Toast.LENGTH_SHORT).show()
                false
            }

    private fun successPairing() {
        val intent = Intent(activity, BluetoothLeService::class.java).apply {
            action = BluetoothStateReceiver.DEVICE_SCAN
            putExtra(BluetoothLeService.EXTRA_DEVICE, deviceModel)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity!!.startForegroundService(intent)
        } else {
            activity!!.startService(intent)
        }
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            deviceModel.state = true
            viewModel.insertDevice(deviceModel)
            try {
                view!!.findNavController().navigate(R.id.action_pairingSuccessFragment_to_actionHome)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
    }
}
