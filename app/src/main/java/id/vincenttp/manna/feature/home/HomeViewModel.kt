package id.vincenttp.manna.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.interactor.device.GetDevices
import id.vincenttp.domain.interactor.device.InsertDevice
import id.vincenttp.manna.base.BaseViewModel

class HomeViewModel(
        private val getDevices: GetDevices,
        private val insertDevice: InsertDevice
) : BaseViewModel() {
    private val _deviceList = MutableLiveData<List<DeviceModel>>()
    val deviceList: LiveData<List<DeviceModel>>
        get() = _deviceList

    init {
        getDevice()
    }

    fun getDevice() {
        getDevices.addParams(null)
                .onSuccess { _deviceList.postValue(it) }
                .onError { it.printStackTrace() }
                .execute(viewModelScope)
    }

    fun insertDevice(deviceModel: DeviceModel) {
        insertDevice
                .addParams(InsertDevice.Params(deviceModel))
                .onSuccess {
                    getDevice()
                }
                .onError {
                    it.printStackTrace()
                    println("onError")
                }
                .execute(viewModelScope)
    }
}
