package id.vincenttp.manna.feature.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.GoogleAuthProvider
import id.vincenttp.manna.R
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentLoginBinding
import id.vincenttp.manna.feature.HomeActivity
import id.vincenttp.manna.feature.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<LoginViewModel>() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    override val viewModel by viewModel<LoginViewModel>()

    lateinit var callbackManager: CallbackManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentLoginBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        tvAuth.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        btnAuth.setOnClickListener {
            startActivity(Intent(context, HomeActivity::class.java))
        }

        ibGoogle.setOnClickListener {
            signInGoogle()
        }
        ibFacebook.setOnClickListener {
            lbFacebook.performClick()
        }

        callbackManager = CallbackManager.Factory.create()

        lbFacebook.setReadPermissions("email", "public_profile")
        lbFacebook.fragment = this
        lbFacebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                println("registerCallback onSuccess")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                println("registerCallback onCancel")
            }

            override fun onError(error: FacebookException) {
                error.printStackTrace()
                println("registerCallback onError")
            }
        })

        tvAuth.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        btnAuth.setOnClickListener {
            doLogin()
        }
        ibGoogle.setOnClickListener {
            signInGoogle()
        }
        ibFacebook.setOnClickListener {
            lbFacebook.performClick()
        }
    }

    override fun onInitObservers() {
        super.onInitObservers()
        viewModel.activity = activity!!
        viewModel.registerForm.email.observe(this, Observer {
            viewModel.authModel.email = it
            if (it.isEmpty()) {
                tilEmail.error = "Email Tidak Boleh Kosong"
            } else {
                tilEmail.error = null
            }
        })
        viewModel.registerForm.password.observe(this, Observer {
            viewModel.authModel.password = it
            if (it.isEmpty()) {
                tilPassword.error = "Password Tidak Boleh Kosong"
            } else {
                tilPassword.error = null
            }
        })
    }

    fun doLogin() {
        if (validateForm()) {
            dialogLoading.show()
            firebaseAuth.signInWithEmailAndPassword(viewModel.authModel.email, viewModel.authModel.password)
                    .addOnCompleteListener(activity!!) { task ->
                        dialogLoading.dismiss()
                        if (task.isSuccessful) {
                            (activity as MainActivity).navigateToHome()
                        } else {
                            println(task.exception)
                            when (task.exception) {
                                is FirebaseAuthInvalidCredentialsException -> {
                                    tilPassword.error = task.exception!!.message
                                }
                                is FirebaseAuthInvalidUserException -> {
                                    tilEmail.error = task.exception!!.message
                                }
                                else -> {
                                    toast(task.exception!!.message!!)
                                }
                            }
                        }
                    }
        } else {
            toast("Silahkan lengkapi form")
        }
    }

    private fun validateForm(): Boolean = (
            viewModel.authModel.email.isNotEmpty()
                    && viewModel.authModel.password.isNotEmpty()
            )

    private fun signInGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("178678444877-8i32a6mgkboof4e6bi64q5kb131netug.apps.googleusercontent.com")
                .requestEmail()
                .build()
        val googleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, 10)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                e.printStackTrace()
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = firebaseAuth.currentUser
                        println(user)
                        (activity as MainActivity).navigateToHome()
                    } else {
                        it.exception!!.printStackTrace()
                    }
                }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = firebaseAuth.currentUser
                        println(user)
                        (activity as MainActivity).navigateToHome()
                    } else {
                        it.exception!!.printStackTrace()
                    }
                }
    }
}
