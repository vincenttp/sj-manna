package id.vincenttp.manna.feature.notif

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.manna.databinding.ItemNotifBinding

/**
 * Created by vincenttp on 2019-08-29.
 */
class NotifAdapter(private val onItemClicked: (NotifModel) -> Unit) : ListAdapter<NotifModel, NotifAdapter.Item>(NotifDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        return Item(ItemNotifBinding.inflate(LayoutInflater.from(parent.context), parent, false), onItemClicked)
    }

    override fun onBindViewHolder(holder: Item, position: Int) = holder.bind(getItem(position))

    class Item(private val binding: ItemNotifBinding, private val onItemClicked: (NotifModel) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: NotifModel) {
            binding.ivDevice.setImageBitmap(BitmapFactory.decodeFile(model.image))
            binding.clickListener = View.OnClickListener {
                model.run(onItemClicked)
            }
            binding.apply {
                binding.model = model
            }
        }
    }
}

private class NotifDiffCallback : DiffUtil.ItemCallback<NotifModel>() {
    override fun areItemsTheSame(oldItem: NotifModel, newItem: NotifModel): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: NotifModel, newItem: NotifModel): Boolean {
        return oldItem == newItem
    }
}