package id.vincenttp.manna.feature.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BluetoothStateReceiver : BroadcastReceiver() {
    companion object{
        const val DEVICE_CONNECTED = "id.vincenttp.manna.connected"
        const val DEVICE_DISCONNECTED = "id.vincenttp.manna.disconnected"
        const val DEVICE_RING_PHONE = "id.vincenttp.manna.ring.phone"
        const val DEVICE_STOP_PHONE = "id.vincenttp.manna.stop.ring.phone"
        const val DEVICE_SCAN = "id.vincenttp.manna.scan"
        const val DEVICE_RING = "id.vincenttp.manna.ring"
        const val DEVICE_STOP_RINGTONE = "id.vincenttp.manna.stop.ringtone"
        const val DEVICE_UPDATE = "id.vincenttp.manna.update"
    }

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        TODO("BluetoothStateReceiver.onReceive() is not implemented")
    }
}
