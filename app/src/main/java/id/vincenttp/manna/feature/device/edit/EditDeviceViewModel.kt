package id.vincenttp.manna.feature.device.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.interactor.device.DeleteDevice
import id.vincenttp.domain.interactor.device.InsertDevice

class EditDeviceViewModel(private val updateDevice: InsertDevice, private val deleteDevice: DeleteDevice) : ViewModel() {
    private val _deviceList = MutableLiveData<Unit>()
    val deviceList: LiveData<Unit>
        get() = _deviceList

    private val _deleteDeviceData = MutableLiveData<Int>()
    val deleteDeviceData: LiveData<Int>
        get() = _deleteDeviceData

    fun updateDevice(deviceModel: DeviceModel) {
        updateDevice.addParams(InsertDevice.Params(deviceModel))
                .onSuccess {
                    _deviceList.postValue(it)
                }
                .execute(viewModelScope)
    }

    fun deleteDevice(deviceModel: DeviceModel){
        deleteDevice.addParams(DeleteDevice.Params(deviceModel))
                .onSuccess { _deleteDeviceData.postValue(it) }
                .execute(viewModelScope)
    }
}
