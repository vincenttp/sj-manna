package id.vincenttp.manna.feature.auth.register

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.*
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentRegisterBinding
import id.vincenttp.manna.feature.MainActivity
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment<RegisterViewModel>() {

    companion object {
        fun newInstance() = RegisterFragment()
    }

    override val viewModel by viewModel<RegisterViewModel>()
    lateinit var callbackManager: CallbackManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentRegisterBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        callbackManager = CallbackManager.Factory.create()

        lbFacebook.setReadPermissions("email", "public_profile")
        lbFacebook.fragment = this
        lbFacebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                println("registerCallback onSuccess")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                println("registerCallback onCancel")
            }

            override fun onError(error: FacebookException) {
                println("registerCallback onError")
            }
        })

        btnAuth.setOnClickListener {
            doRegister()
        }
        tvAuth.setOnClickListener {
            activity!!.onBackPressed()
        }
        ibGoogle.setOnClickListener {
            signInGoogle()
        }
        ibFacebook.setOnClickListener {
            lbFacebook.performClick()
        }
    }

    override fun onInitObservers() {
        super.onInitObservers()
        viewModel.activity = activity!!
        viewModel.registerForm.name.observe(this, Observer {
            viewModel.authModel.name = it
            if (it.isEmpty()) {
                tilName.error = "Nama Tidak Boleh Kosong"
            } else {
                tilName.error = null
            }
        })
        viewModel.registerForm.email.observe(this, Observer {
            viewModel.authModel.email = it
            if (it.isEmpty()) {
                tilEmail.error = "Email Tidak Boleh Kosong"
            } else {
                tilEmail.error = null
            }
        })
        viewModel.registerForm.password.observe(this, Observer {
            viewModel.authModel.password = it
            if (it.isEmpty()) {
                tilPassword.error = "Password Tidak Boleh Kosong"
            } else {
                tilPassword.error = null
            }
        })
        viewModel.registerForm.rePassword.observe(this, Observer {
            viewModel.authModel.rePassword = it
            if (it.isEmpty()) {
                tilReTypePassword.error = "Password Tidak Boleh Kosong"
            } else {
                tilReTypePassword.error = null
            }
            if (viewModel.authModel.password != it) {
                tilReTypePassword.error = "Password Tidak Sama"
            } else {
                tilReTypePassword.error = null
            }
        })
    }

    private fun doRegister() {
        if (validateForm()) {
            dialogLoading.show()
            firebaseAuth.createUserWithEmailAndPassword(viewModel.authModel.email, viewModel.authModel.password)
                    .addOnCompleteListener(activity!!) { task ->
                        dialogLoading.dismiss()
                        if (task.isSuccessful) {
                            updateUser(firebaseAuth.currentUser!!)
                        } else {
                            println(task.exception)
                            when (task.exception) {
                                is FirebaseAuthWeakPasswordException -> {
                                    tilPassword.error = task.exception!!.message
                                }
                                is FirebaseAuthInvalidCredentialsException -> {
                                    tilEmail.error = task.exception!!.message
                                }
                                else -> {
                                    toast(task.exception!!.message!!)
                                }
                            }
                        }
                    }
        } else {
            if (viewModel.authModel.email.isEmpty()) {
                tilEmail.error = "Email required"
            }
            if (viewModel.authModel.name.isEmpty()) {
                tilName.error = "Nama required"
            }
            if (viewModel.authModel.password.isEmpty()) {
                tilPassword.error = "Password required"
            }

            if (viewModel.authModel.password != viewModel.authModel.rePassword) {
                tilReTypePassword.error = "Password not match"
            }
        }
    }

    private fun validateForm(): Boolean = (
            viewModel.authModel.email.isNotEmpty()
                    && viewModel.authModel.name.isNotEmpty()
                    && viewModel.authModel.password.isNotEmpty()
                    && viewModel.authModel.rePassword.isNotEmpty()
            ) && (viewModel.authModel.password == viewModel.authModel.rePassword)

    private fun signInGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("178678444877-8i32a6mgkboof4e6bi64q5kb131netug.apps.googleusercontent.com")
                .requestEmail()
                .build()
        val googleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, 10)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                e.printStackTrace()
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = firebaseAuth.currentUser
                        println(user)
                        (activity as MainActivity).navigateToHome()
                    } else {
                        it.exception!!.printStackTrace()
                    }
                }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = firebaseAuth.currentUser
                        println(user)
                        (activity as MainActivity).navigateToHome()
                    } else {
                        it.exception!!.printStackTrace()
                    }
                }
    }

    private fun updateUser(user: FirebaseUser) {
        val profileUpdates = UserProfileChangeRequest.Builder()
                .setDisplayName(viewModel.authModel.name)
                .build()

        user.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        (activity as MainActivity).navigateToHome()
                    }
                }
    }
}
