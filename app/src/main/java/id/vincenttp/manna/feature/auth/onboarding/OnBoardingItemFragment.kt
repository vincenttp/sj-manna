package id.vincenttp.manna.feature.auth.onboarding


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.vincenttp.manna.R
import kotlinx.android.synthetic.main.fragment_on_boarding_item.*

/**
 * A simple [Fragment] subclass.
 */
class OnBoardingItemFragment : Fragment() {

    companion object {
        const val IMAGES = "IMAGES"
        const val TITLE = "TITLE"
        const val DESCRIPTION = "DESCRIPTION"
        fun newInstance(img: Int, title: String, desc: String): OnBoardingItemFragment {
            val fragment = OnBoardingItemFragment()
            val bundle = Bundle().apply {
                putString(TITLE, title)
                putString(DESCRIPTION, desc)
                putInt(IMAGES, img)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_on_boarding_item, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivOnBoarding.setImageResource(arguments!!.getInt(IMAGES))
        tvTitle.text = arguments!!.getString(TITLE)
        tvDesc.text = arguments!!.getString(DESCRIPTION)
    }
}
