package id.vincenttp.manna.feature.notif

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.domain.interactor.notif.GetNotifs
import id.vincenttp.manna.base.BaseViewModel

class NotifViewModel(val getNotifs: GetNotifs) : BaseViewModel() {
    private val _notifList = MutableLiveData<List<NotifModel>>()
    val notifList: LiveData<List<NotifModel>>
        get() = _notifList

    init {
        getNotifs
                .addParams(null)
                .onSuccess { _notifList.postValue(it) }
                .execute(viewModelScope)
    }
}
