package id.vincenttp.manna.feature.profile.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest

class EditViewModel(private val auth: FirebaseAuth) : ViewModel() {
    private val _user = MutableLiveData<FirebaseUser>()
    val user: LiveData<FirebaseUser>
        get() = _user

    private val _saveListener = MutableLiveData<String>()
    val saveListener: LiveData<String>
        get() = _saveListener

    init {
        _user.value = auth.currentUser
    }

    fun editProfile(name: String) {
        val profileUpdates = UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build()
        auth.currentUser!!.updateProfile(profileUpdates)
                .addOnFailureListener { _saveListener.postValue(it.message) }
                .addOnSuccessListener { _saveListener.postValue("Profile Berhasil di Update") }
    }
}
