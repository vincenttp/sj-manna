package id.vincenttp.manna.feature.add.pairing

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.databinding.FragmentPairingBinding
import id.vincenttp.manna.feature.service.BluetoothLeService
import id.vincenttp.manna.feature.service.BluetoothStateReceiver
import kotlinx.android.synthetic.main.fragment_pairing.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PairingFragment : BaseFragment<PairingViewModel>(), Listener {

    companion object {
        fun newInstance() = PairingFragment()
    }

    override val viewModel by viewModel<PairingViewModel>()

    private val deviceAdapter = DeviceAdapter(this)
    private val deviceList = mutableListOf<DeviceModel>()

    private val bluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = activity!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }
    private val bluetoothLeScanner by lazy {
        bluetoothAdapter!!.bluetoothLeScanner
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentPairingBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.rvDevice.adapter = deviceAdapter
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        deviceAdapter.submitList(deviceList)
        ibBack.setOnClickListener {
            it.findNavController().navigateUp()
        }
    }

    override fun onInitObservers() {
        super.onInitObservers()
        bluetoothLeScanner.startScan(leScanCallback)
    }

    override fun onDestroyView() {
        bluetoothLeScanner.stopScan(leScanCallback)
        super.onDestroyView()
    }

    private val leScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            rvDevice?.let { it.visibility = View.VISIBLE }
            groupWaiting?.let { it.visibility = View.GONE }

            println(result)
            val model = DeviceModel(
                    when {
                        result.device.name == null -> "N/A"
                        result.device.name == "iTrack" -> "Sini Device"
                        else -> result.device.name
                    },
                    result.device.address,
                    false,
                    false,
                    ""
            )
            if (!deviceList.contains(model) && model.name != "N/A") {
                deviceList.add(model)
            }
            deviceAdapter.notifyDataSetChanged()
        }

        override fun onScanFailed(errorCode: Int) {
            println("onScanResult onScanFailed $errorCode")
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            println("onScanResult onBatchScanResults $results")
        }
    }

    override fun onClick(deviceModel: DeviceModel) {
        bluetoothLeScanner.stopScan(leScanCallback)
        view!!.findNavController().navigate(PairingFragmentDirections.actionPairingFragmentToPairingSuccessFragment(deviceModel))
    }
}
