package id.vincenttp.manna.feature.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.RingtoneManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.R
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentHomeBinding
import id.vincenttp.manna.feature.service.BluetoothLeService
import id.vincenttp.manna.feature.service.BluetoothStateReceiver
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeViewModel>() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override val viewModel by viewModel<HomeViewModel>()

    private val adapter = DeviceAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.rvDevice.adapter = adapter
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        ibUser.setOnClickListener {
            it.findNavController().navigate(R.id.action_actionHome_to_profileFragment)
        }
    }

    override fun onInitObservers() {
        super.onInitObservers()
        activity?.let {
            LocalBroadcastManager
                    .getInstance(it)
                    .registerReceiver(
                            receiver,
                            IntentFilter().apply {
                                addAction(BluetoothStateReceiver.DEVICE_DISCONNECTED)
                                addAction(BluetoothStateReceiver.DEVICE_CONNECTED)
                                addAction(BluetoothStateReceiver.DEVICE_RING_PHONE)
                            })
        }
        viewModel.deviceList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getDevice()
    }

    override fun onDestroyView() {
        activity?.let { LocalBroadcastManager.getInstance(it).unregisterReceiver(receiver) }
        super.onDestroyView()
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val deviceModel = p1!!.getSerializableExtra(BluetoothLeService.EXTRA_DEVICE) as DeviceModel
            if (p1.action == BluetoothStateReceiver.DEVICE_RING_PHONE) {
                val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val r = RingtoneManager.getRingtone(context, notification)
                r.play()
                toast("Device Click")
            } else {
                viewModel.insertDevice(deviceModel)
            }
        }

    }
}
