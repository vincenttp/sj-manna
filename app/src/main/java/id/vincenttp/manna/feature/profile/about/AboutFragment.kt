package id.vincenttp.manna.feature.profile.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.vincenttp.manna.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {

    companion object {
        fun newInstance() = AboutFragment()
    }

    private lateinit var viewModel: AboutViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentAboutBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@AboutFragment
            onClickBack = createOnBackClickListener
        }
        return binding.root
    }

    private val createOnBackClickListener = View.OnClickListener {
        activity!!.onBackPressed()
    }

}
