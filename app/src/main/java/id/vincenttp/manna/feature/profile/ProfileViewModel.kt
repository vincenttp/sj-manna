package id.vincenttp.manna.feature.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import id.vincenttp.manna.base.BaseViewModel

class ProfileViewModel(auth: FirebaseAuth) : BaseViewModel() {
    private val _user = MutableLiveData<FirebaseUser>()
    val user: LiveData<FirebaseUser>
        get() = _user

    init {
        _user.value = auth.currentUser
    }
}
