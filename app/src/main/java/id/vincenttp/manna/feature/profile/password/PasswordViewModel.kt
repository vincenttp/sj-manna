package id.vincenttp.manna.feature.profile.password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class PasswordViewModel(private val auth: FirebaseAuth) : ViewModel() {
    private val _saveListener = MutableLiveData<String>()
    val saveListener: LiveData<String>
        get() = _saveListener

    val passwordForm by lazy {
        PasswordForm()
    }
    val passwordModel by lazy {
        PasswordModel()
    }

    fun changePassword(){
        auth.currentUser!!.updatePassword(passwordModel.newPassword)
                .addOnSuccessListener { _saveListener.postValue("Password Berhasil di Ubah") }
                .addOnFailureListener { _saveListener.postValue(it.message) }
    }
}
