package id.vincenttp.manna.feature.auth.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import id.vincenttp.manna.R

/**
 * Created by vincenttp on 2019-09-23.
 */
class OnBoardingAdapter(fm: FragmentManager, behavior: Int) : FragmentStatePagerAdapter(fm, behavior) {

    private val title = listOf(
            "Welcome to SINI app!",
            "SINI app will connect with your SINI device to find everything that matters",
            "No more slipping, get more tracking\u000BLet's get started !"
    )

    private val decs = listOf(
            "SINI is a startup that inspired by the people closest to us who often have problems that are often forgotten when placing an item and have had the experience of losing important items such as keys, wallets, and passports.",
            "SINI providing one of the best solutions to solve that problem, because our first product is SINI Device Tracker.",
            "SINI Device Tracker is a product that can be used to help us to find our own belongings when we lose them, and this product also equipped by SINI Apps that can be installed by our product user in their smartphone."
    )

    private val images = listOf(
            R.drawable.ic_logo,
            R.drawable.img_onboarding_1,
            R.drawable.img_onboarding_2
    )

    override fun getItem(position: Int): Fragment {
        return OnBoardingItemFragment.newInstance(images[position], title[position], decs[position])
    }

    override fun getCount(): Int = 3
}