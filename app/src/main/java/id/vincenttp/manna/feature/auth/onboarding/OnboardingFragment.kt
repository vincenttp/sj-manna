package id.vincenttp.manna.feature.auth.onboarding


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
import androidx.navigation.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.firebase.auth.FirebaseAuth
import id.vincenttp.manna.databinding.FragmentOnboardingBinding
import id.vincenttp.manna.feature.MainActivity
import kotlinx.android.synthetic.main.fragment_onboarding.*
import org.koin.android.ext.android.inject
import id.vincenttp.manna.R

/**
 * A simple [Fragment] subclass.
 */
class OnboardingFragment : Fragment(), ViewPager.OnPageChangeListener, View.OnClickListener {

    val firebaseAuth: FirebaseAuth by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.vpOnBoarding.adapter = OnBoardingAdapter(fragmentManager!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
        binding.vpOnBoarding.offscreenPageLimit = 3
        binding.vpOnBoarding.addOnPageChangeListener(this)
        binding.btnNext.setOnClickListener(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (firebaseAuth.currentUser != null) {
            (activity as MainActivity).navigateToHome()
        }
        /*btnNext.setOnClickListener {

        }*/
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if (position==2){
            btnNext.text = "DONE"
        }else{
            btnNext.text = "NEXT"
        }
    }

    override fun onClick(p0: View?) {
        if (vpOnBoarding.currentItem == 2){
            p0!!.findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
        }else{
            val itemPos = vpOnBoarding.currentItem+1
            vpOnBoarding.postDelayed({
                vpOnBoarding.currentItem = itemPos
            }, 100)
        }
    }
}
