package id.vincenttp.manna.feature.auth

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData

/**
 * Created by vincenttp on 2019-08-10.
 */

data class AuthModel(
        var email: String = "",
        var password: String = "",
        var name: String = "",
        var rePassword: String = ""
)

class RegisterForm : BaseObservable() {
    @Bindable
    val email = MutableLiveData<String>()

    @Bindable
    val password = MutableLiveData<String>()

    @Bindable
    val name = MutableLiveData<String>()

    @Bindable
    val rePassword = MutableLiveData<String>()
}