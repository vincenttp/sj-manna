package id.vincenttp.manna.feature.profile.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentEditBinding
import kotlinx.android.synthetic.main.fragment_edit.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditFragment : Fragment() {

    companion object {
        fun newInstance() = EditFragment()
    }

    private val viewModel by viewModel<EditViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentEditBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.onClickBack = createOnBackClickListener
        binding.onClickSave = createOnSaveClickListener
        return binding.root
    }

    private val createOnBackClickListener = View.OnClickListener {
        activity!!.onBackPressed()
    }

    private val createOnSaveClickListener = View.OnClickListener {
        viewModel.editProfile(etName.text.toString())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.saveListener.observe(this, Observer {
            toast(it)
        })
    }
}
