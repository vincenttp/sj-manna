package id.vincenttp.manna.feature.auth.register

import android.app.Activity
import id.vincenttp.manna.base.BaseViewModel
import id.vincenttp.manna.feature.auth.AuthModel
import id.vincenttp.manna.feature.auth.RegisterForm


class RegisterViewModel() : BaseViewModel() {
    lateinit var activity: Activity

    val registerForm by lazy {
        RegisterForm()
    }
    val authModel by lazy {
        AuthModel()
    }
}
