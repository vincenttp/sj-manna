package id.vincenttp.manna.feature.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.facebook.login.LoginManager
import id.vincenttp.manna.R
import id.vincenttp.manna.base.BaseFragment
import id.vincenttp.manna.databinding.FragmentProfileBinding
import id.vincenttp.manna.feature.MainActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<ProfileViewModel>() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    override val viewModel by viewModel<ProfileViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.onCLickAbout = createOnClickAboutListener
        binding.onCLickEdit = createOnClickEditListener
        binding.onCLickPassword = createOnClickPasswordListener
        binding.onClickNotif = createOnClickNotif
        return binding.root
    }

    override fun onInitViews() {
        super.onInitViews()
        ibBack.setOnClickListener {
            activity!!.onBackPressed()
        }

        btnLogout.setOnClickListener {
            firebaseAuth.signOut()
            LoginManager.getInstance().logOut();
            startActivity(Intent(context, MainActivity::class.java))
            activity!!.finish()
        }
    }

    private val createOnClickAboutListener = View.OnClickListener {
        it.findNavController().navigate(R.id.action_profileFragment_to_aboutFragment)
    }

    private val createOnClickEditListener = View.OnClickListener {
        it.findNavController().navigate(R.id.action_profileFragment_to_editFragment)
    }

    private val createOnClickPasswordListener = View.OnClickListener {
        it.findNavController().navigate(R.id.action_profileFragment_to_passwordFragment)
    }

    private val createOnClickNotif = View.OnClickListener {
        it.findNavController().navigate(R.id.action_profileFragment_to_actionNotification)
    }
}
