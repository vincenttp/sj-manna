package id.vincenttp.manna.feature.profile.password

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData

/**
 * Created by vincenttp on 2019-10-23.
 */

data class PasswordModel(
        var newPassword: String = "",
        var rePassword: String = ""
)

class PasswordForm : BaseObservable() {
    @Bindable
    val newPassword = MutableLiveData<String>()

    @Bindable
    val rePassword = MutableLiveData<String>()
}