package id.vincenttp.manna.feature.device.edit

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.R
import id.vincenttp.manna.common.ImageFilePath
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentEditDeviceBinding
import id.vincenttp.manna.feature.service.BluetoothLeService
import id.vincenttp.manna.feature.service.BluetoothLeService.Companion.EXTRA_DEVICE
import id.vincenttp.manna.feature.service.BluetoothStateReceiver
import kotlinx.android.synthetic.main.fragment_edit_device.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class EditDeviceFragment : Fragment() {

    companion object {
        fun newInstance() = EditDeviceFragment()

        const val RESULT_CODE_GALLERY = 19
    }

    private val viewModel by viewModel<EditDeviceViewModel>()

    private val args: EditDeviceFragmentArgs by navArgs()

    private var imageUri = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentEditDeviceBinding.inflate(inflater, container, false)
        binding.model = args.model
        binding.onClickClose = createOnClickClose
        binding.onClickSave = createOnClickSave
        binding.onClickDelete = createOnClickDelete
        binding.onClickImage = createOnClickImage
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        imageUri = args.model.image
        ivDevice.setImageBitmap(BitmapFactory.decodeFile(args.model.image))
        viewModel.deviceList.observe(this, Observer {
            toast("Berhasil Mengubah Device")
        })

        viewModel.deleteDeviceData.observe(this, Observer {
            view!!.findNavController().navigate(R.id.action_editDeviceFragment_to_actionHome)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_CODE_GALLERY && resultCode == Activity.RESULT_OK) {
            imageUri = File(ImageFilePath.getPath(context, data!!.data)).absolutePath
            ivDevice.setImageBitmap(BitmapFactory.decodeFile(imageUri))
        }
    }

    private val createOnClickClose = View.OnClickListener {
        activity!!.onBackPressed()
    }

    private val createOnClickSave = View.OnClickListener {
        val deviceViewModel = DeviceModel(etDeviceName.text.toString(), args.model.address, args.model.state, args.model.isRinging, imageUri)
        viewModel.updateDevice(deviceViewModel)
        val intent = Intent(activity, BluetoothLeService::class.java).apply {
            action = BluetoothStateReceiver.DEVICE_UPDATE
            putExtra(EXTRA_DEVICE, deviceViewModel)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity!!.startForegroundService(intent)
        } else {
            activity!!.startService(intent)
        }
    }

    private val createOnClickDelete = View.OnClickListener {
        val deviceViewModel = DeviceModel(etDeviceName.text.toString(), args.model.address, args.model.state, args.model.isRinging, imageUri)
        activity!!.startService(
                Intent(activity, BluetoothLeService::class.java).apply {
                    action = BluetoothStateReceiver.DEVICE_DISCONNECTED
                    putExtra(EXTRA_DEVICE, deviceViewModel)
                }
        )
        viewModel.deleteDevice(deviceViewModel)
    }

    private val createOnClickImage = View.OnClickListener {
        if (checkPermissions()) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Pilih Cover"), RESULT_CODE_GALLERY)
        }
    }

    private fun checkPermissions(): Boolean =
            if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 18)
                Toast.makeText(context, "Anda tidak memberikan akses untuk galeri.", Toast.LENGTH_SHORT).show()
                false
            }
}
