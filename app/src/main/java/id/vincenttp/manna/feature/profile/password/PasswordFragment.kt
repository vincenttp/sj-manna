package id.vincenttp.manna.feature.profile.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentPasswordBinding
import kotlinx.android.synthetic.main.fragment_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PasswordFragment : Fragment() {

    private val viewModel by viewModel<PasswordViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentPasswordBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.onClickSave = createOnClickSaveListener
        binding.onClickBack = createOnClickBackListener
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.passwordForm.newPassword.observe(this, Observer {
            viewModel.passwordModel.newPassword = it
            if (it.isEmpty()) {
                tilNewPassword.error = "Password Tidak Boleh Kosong"
            } else {
                tilNewPassword.error = null
            }
        })
        viewModel.passwordForm.rePassword.observe(this, Observer {
            viewModel.passwordModel.rePassword = it
            when {
                it.isEmpty() -> tilReTypePassword.error = "Password Tidak Boleh Kosong"
                viewModel.passwordModel.newPassword != it -> tilReTypePassword.error = "Password Tidak Sama"
                else -> tilReTypePassword.error = null
            }
        })
        viewModel.saveListener.observe(this, Observer {
            toast(it)
        })
    }

    private val createOnClickSaveListener = View.OnClickListener {
        viewModel.changePassword()
    }

    private val createOnClickBackListener = View.OnClickListener {
        activity!!.onBackPressed()
    }
}
