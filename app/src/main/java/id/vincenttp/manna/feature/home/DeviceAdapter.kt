package id.vincenttp.manna.feature.home

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.databinding.ItemDeviceBinding

/**
 * Created by vincenttp on 2019-08-29.
 */
class DeviceAdapter : ListAdapter<DeviceModel, DeviceAdapter.Item>(DeviceDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        return Item(ItemDeviceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Item, position: Int) = holder.bind(getItem(position), createOnClickListener(getItem(position)))

    private fun createOnClickListener(model: DeviceModel) = View.OnClickListener {
        it.findNavController().navigate(HomeFragmentDirections.actionActionHomeToDeviceFragment(model))
    }

    class Item(private val binding: ItemDeviceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: DeviceModel, onClick: View.OnClickListener) {
            binding.ivDevice.setImageBitmap(BitmapFactory.decodeFile(model.image))
            binding.apply {
                binding.model = model
                binding.onClick = onClick
            }
        }
    }
}

private class DeviceDiffCallback : DiffUtil.ItemCallback<DeviceModel>() {
    override fun areItemsTheSame(oldItem: DeviceModel, newItem: DeviceModel): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DeviceModel, newItem: DeviceModel): Boolean {
        return oldItem == newItem
    }
}