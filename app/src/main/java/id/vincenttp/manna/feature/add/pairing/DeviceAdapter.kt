package id.vincenttp.manna.feature.add.pairing

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.databinding.ItemPairingBinding

/**
 * Created by vincenttp on 2019-08-29.
 */
class DeviceAdapter(val listener: Listener) : ListAdapter<DeviceModel, DeviceAdapter.Item>(DeviceDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        return Item(ItemPairingBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Item, position: Int) =
            holder.bind(
                    getItem(position),
                    createOnClickListener(getItem(position))
            )

    private fun createOnClickListener(model: DeviceModel): View.OnClickListener {
        return View.OnClickListener {
            listener.onClick(model)
        }
    }

    class Item(private val binding: ItemPairingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: DeviceModel, onClick: View.OnClickListener) {
            binding.apply {
                binding.clickListener = onClick
                binding.model = model
            }
        }
    }
}

private class DeviceDiffCallback : DiffUtil.ItemCallback<DeviceModel>() {
    override fun areItemsTheSame(oldItem: DeviceModel, newItem: DeviceModel): Boolean {
        return oldItem.address == newItem.address
    }

    override fun areContentsTheSame(oldItem: DeviceModel, newItem: DeviceModel): Boolean {
        return oldItem.address == newItem.address
    }
}

interface Listener {
    fun onClick(deviceModel: DeviceModel)
}