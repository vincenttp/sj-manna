package id.vincenttp.manna.feature.device

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.manna.common.extention.toast
import id.vincenttp.manna.databinding.FragmentDeviceBinding
import id.vincenttp.manna.feature.service.BluetoothLeService
import id.vincenttp.manna.feature.service.BluetoothStateReceiver
import kotlinx.android.synthetic.main.fragment_device.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DeviceFragment : Fragment() {

    companion object {
        fun newInstance() = DeviceFragment()
    }

    private val viewModel by viewModel<DeviceViewModel>()

    private val args: DeviceFragmentArgs by navArgs()

    lateinit var deviceModel: DeviceModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentDeviceBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.onClickEdit = createOnClickEdit
        binding.onClickSound = createOnClickPlaySound
        binding.onClickConnection = createOnClickConnection
        binding.onClickBack = createOnClickBack
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.getDevice(args.model.address)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            LocalBroadcastManager
                    .getInstance(it)
                    .registerReceiver(
                            receiver,
                            IntentFilter().apply {
                                addAction(BluetoothStateReceiver.DEVICE_UPDATE)
                            })
        }
        viewModel.deviceList.observe(this, Observer {
            this.deviceModel = it
            ivDevice.setImageBitmap(BitmapFactory.decodeFile(it.image))
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.let { LocalBroadcastManager.getInstance(it).unregisterReceiver(receiver) }
    }

    private val createOnClickEdit = View.OnClickListener {
        it.findNavController().navigate(DeviceFragmentDirections.actionDeviceFragmentToEditDeviceFragment(viewModel.deviceList.value!!))
    }

    private val createOnClickPlaySound = View.OnClickListener {
        activity!!.startService(
                Intent(activity, BluetoothLeService::class.java).apply {
                    action = BluetoothStateReceiver.DEVICE_RING
                }
        )
    }

    private val createOnClickConnection = View.OnClickListener {
        activity!!.startService(
                Intent(activity, BluetoothLeService::class.java).apply {
                    action = if (deviceModel.state) BluetoothStateReceiver.DEVICE_DISCONNECTED else BluetoothStateReceiver.DEVICE_CONNECTED
                    putExtra(BluetoothLeService.EXTRA_DEVICE, deviceModel)
                }
        )
    }

    private val createOnClickBack = View.OnClickListener {
        activity!!.onBackPressed()
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val deviceModel = p1!!.getSerializableExtra(BluetoothLeService.EXTRA_DEVICE) as DeviceModel
            viewModel._deviceList.value = deviceModel
        }

    }
}
