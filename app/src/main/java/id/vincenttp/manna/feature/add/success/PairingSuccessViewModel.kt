package id.vincenttp.manna.feature.add.success

import androidx.lifecycle.viewModelScope
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.interactor.device.InsertDevice
import id.vincenttp.manna.base.BaseViewModel

class PairingSuccessViewModel(private val insertDevice: InsertDevice) : BaseViewModel() {

    fun insertDevice(deviceModel: DeviceModel) {
        insertDevice
                .addParams(InsertDevice.Params(deviceModel))
                .onSuccess { println("onSuccess") }
                .onError {
                    it.printStackTrace()
                    println("onError")
                }
                .execute(viewModelScope)
    }
}
