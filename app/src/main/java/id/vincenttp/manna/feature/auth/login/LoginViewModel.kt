package id.vincenttp.manna.feature.auth.login

import android.app.Activity
import id.vincenttp.manna.base.BaseViewModel
import id.vincenttp.manna.feature.auth.AuthModel
import id.vincenttp.manna.feature.auth.RegisterForm

class LoginViewModel() : BaseViewModel() {
    lateinit var activity: Activity

    val registerForm by lazy {
        RegisterForm()
    }
    val authModel by lazy {
        AuthModel()
    }
}
