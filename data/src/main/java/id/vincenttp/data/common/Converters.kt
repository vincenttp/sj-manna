package id.vincenttp.data.common

import androidx.room.TypeConverter
import java.util.*

/**
 * Created by vincenttp on 2019-09-28.
 */
class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}