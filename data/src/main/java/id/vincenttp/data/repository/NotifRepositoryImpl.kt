package id.vincenttp.data.repository

import id.vincenttp.data.common.Converters
import id.vincenttp.data.datasource.dao.NotifDao
import id.vincenttp.data.table.NotifTable
import id.vincenttp.domain.entities.NotifModel
import id.vincenttp.domain.repository.NotifRepository

/**
 * Created by vincenttp on 2019-09-28.
 */
class NotifRepositoryImpl(private val dao: NotifDao) : NotifRepository {
    override suspend fun gets(): List<NotifModel> =
            dao.gets().map {
                NotifModel(
                        it.id,
                        it.address,
                        it.message,
                        it.image,
                        it.lat,
                        it.lng,
                        Converters().dateToTimestamp(it.date)!!
                )
            }

    override fun insert(notifModel: NotifModel) {
        val notifTable = NotifTable(
                0,
                notifModel.address,
                notifModel.message,
                notifModel.image,
                notifModel.lat,
                notifModel.lng,
                Converters().fromTimestamp(System.currentTimeMillis())!!
        )
        dao.insert(notifTable)
    }
}