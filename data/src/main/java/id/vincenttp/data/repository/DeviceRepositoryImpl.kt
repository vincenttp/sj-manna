package id.vincenttp.data.repository

import id.vincenttp.data.datasource.dao.DeviceDao
import id.vincenttp.data.table.DeviceTable
import id.vincenttp.domain.entities.DeviceModel
import id.vincenttp.domain.repository.DeviceRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * Created by vincenttp on 2019-09-02.
 */
class DeviceRepositoryImpl(val dao: DeviceDao) : DeviceRepository {
    override fun changeDevice(deviceModel: DeviceModel) {val deviceTable = DeviceTable()
        deviceTable.state = deviceModel.state
        deviceTable.name = deviceModel.name
        deviceTable.address = deviceModel.address
        deviceTable.isRinging = deviceModel.isRinging
        deviceTable.image = deviceModel.image
        dao.update(deviceTable)
    }

    override suspend fun deleteDevice(deviceModel: DeviceModel): Int {
        val deviceTable = DeviceTable()
        deviceTable.state = deviceModel.state
        deviceTable.name = deviceModel.name
        deviceTable.address = deviceModel.address
        deviceTable.isRinging = deviceModel.isRinging
        deviceTable.image = deviceModel.image
        return dao.delete(deviceTable)
    }

    override suspend fun updateDevice(deviceModel: DeviceModel): Job {
        val deviceTable = DeviceTable()
        deviceTable.state = deviceModel.state
        deviceTable.name = deviceModel.name
        deviceTable.address = deviceModel.address
        deviceTable.isRinging = deviceModel.isRinging
        deviceTable.image = deviceModel.image
        return CoroutineScope(Dispatchers.IO).launch {
            dao.update(deviceTable)
        }
    }

    override suspend fun getDevice(address: String): DeviceModel {
        val deviceTable = dao.select(address)
        return DeviceModel(deviceTable.name, deviceTable.address, deviceTable.state, deviceTable.isRinging, deviceTable.image)
    }

    override suspend fun getDevices(): List<DeviceModel> = dao.gets().map {
        DeviceModel(
                it.name,
                it.address,
                it.state,
                it.isRinging,
                it.image
        )
    }

    override suspend fun insertDevice(deviceModel: DeviceModel) {
        val deviceTable = DeviceTable()
        deviceTable.address = deviceModel.address
        deviceTable.name = deviceModel.name
        deviceTable.state = deviceModel.state
        deviceTable.isRinging = deviceModel.isRinging
        deviceTable.image = deviceModel.image
        dao.insert(deviceTable)
    }
}