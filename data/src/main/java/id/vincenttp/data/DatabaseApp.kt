package id.vincenttp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import id.vincenttp.data.common.Converters
import id.vincenttp.data.datasource.dao.DeviceDao
import id.vincenttp.data.datasource.dao.NotifDao
import id.vincenttp.data.table.DeviceTable
import id.vincenttp.data.table.NotifTable

/**
 * Created by vincenttp on 2019-09-02.
 */
@Database(
        entities = [
            DeviceTable::class,
            NotifTable::class
        ],
        version = 6,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class DatabaseApp : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao
    abstract fun notifDao(): NotifDao
}