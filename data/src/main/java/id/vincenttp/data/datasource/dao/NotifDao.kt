package id.vincenttp.data.datasource.dao

import androidx.room.*
import id.vincenttp.data.table.DeviceTable
import id.vincenttp.data.table.NotifTable

/**
 * Created by vincenttp on 2019-09-28.
 */

@Dao
interface NotifDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(notifTable: NotifTable): Long

    @Query("SELECT * from notif ORDER BY date DESC")
    fun gets(): List<NotifTable>

    @Query("SELECT * from device WHERE address = :address")
    fun select(address: String): DeviceTable

    @Update
    fun update(users: DeviceTable)

    @Delete
    fun delete(device: DeviceTable): Int
}