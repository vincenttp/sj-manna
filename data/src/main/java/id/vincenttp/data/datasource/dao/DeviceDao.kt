package id.vincenttp.data.datasource.dao

import androidx.room.*
import id.vincenttp.data.table.DeviceTable

/**
 * Created by vincenttp on 2019-09-02.
 */

@Dao
interface DeviceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(accounts: DeviceTable): Long

    @Query("SELECT * from device")
    fun gets(): List<DeviceTable>

    @Query("SELECT * from device WHERE address = :address")
    fun select(address: String): DeviceTable

    @Update
    fun update(users: DeviceTable)

    @Delete
    fun delete(device: DeviceTable): Int

}