package id.vincenttp.data.table

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Created by vincenttp on 2019-09-08.
 */

@Entity(tableName = "notif")
data class NotifTable(
        @PrimaryKey(autoGenerate = true)
        @NonNull
        var id: Int = 0,
        var address: String,
        var message: String,
        var image: String,
        var lat: Double,
        var lng: Double,
        var date: Date
)