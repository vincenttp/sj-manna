package id.vincenttp.data.table

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by vincenttp on 2019-09-02.
 */

@Entity(tableName = "device")
class DeviceTable {
    @PrimaryKey
    @NonNull
    lateinit var address: String
    lateinit var name: String
    var state: Boolean = false
    var isRinging: Boolean = false
    lateinit var image: String
}